buildscript {
	repositories {
		mavenCentral()
		google()
	}

	dependencies {
		classpath("com.android.tools.build", "gradle", Versions.androidGradle)
		classpath(kotlin("gradle-plugin", Versions.kotlin))
		classpath("com.neenbedankt.gradle.plugins", "android-apt", "1.8")
	}
}

plugins {
	id("com.android.application")
	kotlin("android")
	id("com.google.dagger.hilt.android")
	id("com.google.devtools.ksp")
}

repositories {
	mavenCentral()
	google()
}

android {
	compileSdk = 34

	defaultConfig {
		applicationId = "org.anibyl.slounik"
		minSdk = 21
		targetSdk = 34
		versionCode = 36
		versionName = "2024.03.03.1"
		multiDexEnabled = true
	}

	compileOptions {
		sourceCompatibility = Versions.java
		targetCompatibility = Versions.java
	}

	buildFeatures {
		compose = true
		viewBinding = true
		dataBinding = true
		buildConfig = true
	}

	composeOptions {
		kotlinCompilerExtensionVersion = "1.5.10"
	}

	buildTypes {
		named("release") {
			isMinifyEnabled = false
			proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
		}
		named("debug") {
			isMinifyEnabled = false
		}
	}

	namespace = "org.anibyl.slounik"

	lint {
		checkReleaseBuilds = false
	}

}

dependencies {
	api("androidx.appcompat:appcompat:1.6.1")
	api("androidx.legacy:legacy-support-v4:1.0.0")
	api("com.android.volley:volley:1.2.1")
	api("com.github.castorflex.smoothprogressbar:library:1.1.0")
	api("com.google.code.gson:gson:2.10.1")
	api("org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}")
	api("org.jsoup:jsoup:1.17.2")
	compileOnly("javax.annotation:jsr250-api:1.0")
	implementation("com.android.support:multidex:1.0.3")
	implementation("com.google.android.material:material:1.11.0")

	implementation("androidx.room:room-runtime:2.6.1")
	ksp("androidx.room:room-compiler:2.6.1")

	// Hilt: dependency injection
	implementation("com.google.dagger:hilt-android:${Versions.hilt}")
	ksp("com.google.dagger:hilt-android-compiler:${Versions.hilt}")

	// Jetpack Compose
	implementation(platform("androidx.compose:compose-bom:2024.02.01"))

	implementation("androidx.compose.runtime:runtime:1.6.2")
	implementation("androidx.compose.ui:ui:1.6.2")
	implementation("androidx.compose.foundation:foundation:1.6.2")
	implementation("androidx.compose.foundation:foundation-layout:1.6.2")
	implementation("androidx.compose.material:material:1.6.2")
	implementation("androidx.compose.runtime:runtime-livedata:1.6.2")
	implementation("androidx.compose.ui:ui-tooling:1.6.2")
	implementation("com.google.accompanist:accompanist-themeadapter-material:0.34.0")
	// Jetpack Compose end
}
