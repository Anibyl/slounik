package org.anibyl.slounik.data.network

import android.net.Uri
import com.android.volley.RequestQueue
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.anibyl.slounik.Notifier
import org.anibyl.slounik.core.Preferences
import org.anibyl.slounik.data.Article
import org.anibyl.slounik.data.ArticleBatch
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.nodes.Node
import org.jsoup.nodes.TextNode
import org.jsoup.select.Elements

@Singleton
class SlounikOrgLoader @Inject constructor(
	private val notifier: Notifier,
	private val preferences: Preferences,
	private val queue: RequestQueue,
) : Loader {
	private val domain: String = "slounik.org"

	override fun findArticles(wordToSearch: String): Flow<ArticleBatch> = flow {
		val builder = baseUri()
			.buildUpon()
			.appendPath("search")
			.appendQueryParameter("search", wordToSearch)

		if (preferences.searchInTitles) {
			builder.appendQueryParameter("un", "1")
		}

		var uri: String? = builder.build().toString()
		var page: Page
		do {
			page = getPage(uri!!)
			emit(page.articleBatch)
			uri = page.nextPageUri
		} while (uri != null)
	}.flowOn(Dispatchers.IO)

	private fun getPage(uri: String): Page {
		val isLastPage: Boolean
		val articles: List<Article>
		var nextPageUri: String? = null

		try {
			val response = queue.loadString(uri)

			val page = Jsoup.parse(response)

			val articleElements = page.select("li#res1")

			articles = articleElements.map { e -> parseElement(e) }

			val forwardButton = page.select("a.button-fw").first()
			isLastPage = forwardButton == null

			if (!isLastPage) {
				nextPageUri = baseUri().toString() + forwardButton!!.attr("href")
			}
		} catch (e: Exception) {
			notifier.debug("Error response for $uri: ${e.message}")
			return Page(ArticleBatch())
		}

		return Page(ArticleBatch(articles), nextPageUri)
	}

	private fun parseElement(element: Element): Article {
		return Article().apply {
			var elements: Elements? = element.select("a.link-dark")
			if (elements?.size == 2) {
				// “Link” entry
				val titleLink: Element? = elements.first()
				title = titleLink!!.html()
				val linkToFullDescription = titleLink.attr("href")
					.let { href -> if (href.startsWith("/")) baseUri().toString() + href else href }

				fullDescriptionLoader = {
					val response = queue.loadString(linkToFullDescription)

					val articlePage: Document = Jsoup.parse(response)
					val articleElement: Element = articlePage.select("article").first()!!

					val htmlDescription:String = articleElement.html()
					val descriptionWithOutExtraSpace: String = htmlDescription.trim { it <= ' ' }

					htmlDescription.subSequence(0, descriptionWithOutExtraSpace.length).toString()
				}

				if (title != null) {
					description = "$title ${elements.last()?.html()}"
				}
			}

			if (title == null) {
				elements = element.select("b")
				if (elements.size != 0) {
					// “Text” entry
					title = elements.first()!!.html()

					if (title != null) {
						elements = element.select("div")
						val first = elements.first()!!
						var descriptionElements: List<Node> = first.childNodes().filter { e -> e is Element || e is TextNode }

						val blankToRemove = fun Node.(): Boolean = (this is TextNode && this.isBlank)
						val spanToRemove = fun Node.(): Boolean = (this is Element && this.tagName() == "span")
						val linkToRemove = fun Node.(): Boolean = (this is Element && this.tagName() == "a")

						if (descriptionElements.first().blankToRemove()) {
							descriptionElements = descriptionElements.subList(1, descriptionElements.size - 1)
						}

						if (descriptionElements.last().linkToRemove()) {
							descriptionElements = descriptionElements.subList(0, descriptionElements.size - 2)
						}
						if (descriptionElements.last().blankToRemove()) {
							descriptionElements = descriptionElements.subList(0, descriptionElements.size - 2)
						}
						if (descriptionElements.last().spanToRemove()) {
							descriptionElements = descriptionElements.subList(0, descriptionElements.size - 2)
						}

						val descriptionTexts = descriptionElements.map { e ->
							if (e is Element) {
								if (e.tagName() == "br") {
									e.toString()
								} else {
									e.outerHtml()
								}
							} else if (e is TextNode) {
								e.text()
							} else {
								""
							}
						}

						description = descriptionTexts.joinToString("").trim()
					}
				}
			}

			if (title != null) {
				// Replace underscore with accent
				title = title!!.replace("<u>".toRegex(), "")
				title = title!!.replace("</u>".toRegex(), "́")

				// Escape all other HTML tags, e.g. second <b>.
				title = Jsoup.parse(title!!).text()
			}

			elements = element.select("a.link-light")
			if (elements.isNotEmpty()) {
				dictionary = "${elements.first()!!.html()} $domain"
			}
		}
	}

	private fun baseUri(): Uri {
		return Uri.Builder().scheme("https").authority(domain).build()
	}

	private data class Page(val articleBatch: ArticleBatch, val nextPageUri: String? = null)
}