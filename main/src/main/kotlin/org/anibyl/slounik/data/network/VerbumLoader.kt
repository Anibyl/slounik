package org.anibyl.slounik.data.network

import android.content.Context
import android.net.Uri
import com.android.volley.RequestQueue
import com.google.gson.JsonParser
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.anibyl.slounik.Notifier
import org.anibyl.slounik.R
import org.anibyl.slounik.core.Preferences
import org.anibyl.slounik.data.Article
import org.anibyl.slounik.data.ArticleBatch
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

@Singleton
class VerbumLoader @Inject constructor(
	@ApplicationContext private val context: Context,
	private val notifier: Notifier,
	private val preferences: Preferences,
	private val queue: RequestQueue
) : Loader {
	private val domain: String = "verbum.by"

	override fun findArticles(wordToSearch: String): Flow<ArticleBatch> = flow {
		var page: Page
		var pageNo = 1
		do {
			page = getPage(wordToSearch, pageNo)
			emit(ArticleBatch(page.articles))
			pageNo++
		} while (!page.last)
	}.flowOn(Dispatchers.IO)

	private fun getPage(wordToSearch: String, number: Int): Page {
		val uri = Uri.Builder()
			.scheme("https")
			.authority(domain)
			.appendPath("api")
			.appendPath("search")
			.appendQueryParameter("q", wordToSearch)
			.appendQueryParameter("page", number.toString())
			.build()

		var lastPage = true
		var articles: List<Article>

		try {
			val response = queue.loadString(uri)
			val jsonResponse = JsonParser.parseString(response).asJsonObject

			articles = jsonResponse.get("Articles")
				.asJsonArray
				.map { e -> e.asJsonObject }
				.mapNotNull { e ->
					val description = e.get("Content").asString

					val parsedDescription: Document = Jsoup.parse(description)

					val title = parsedDescription.select("v-hw")
						.ifEmpty { parsedDescription.getElementsByClass("hw") }
						.ifEmpty { parsedDescription.select("strong") }
						.first()
						?.apply { select("sup").remove() }
						?.text()
						?.substringBefore(',')
						?: ""

					if (preferences.searchInTitles && !title.contains(wordToSearch)) {
						return@mapNotNull null
					}

					val dictionary = when (val dictionaryId = e.get("DictionaryID").asString) {
						"abs" -> context.resources.getString(R.string.verbum_abs)
						"brs" -> context.resources.getString(R.string.verbum_brs)
						"esbm" -> context.resources.getString(R.string.verbum_esbm)
						"hsbm" -> context.resources.getString(R.string.verbum_hsbm)
						"kurjanka" -> context.resources.getString(R.string.verbum_kurjanka)
						"pbs" -> context.resources.getString(R.string.verbum_pbs)
						"rbs10" -> context.resources.getString(R.string.verbum_rbs)
						"susha" -> context.resources.getString(R.string.verbum_susha)
						"tsblm" -> context.resources.getString(R.string.verbum_tsblm)
						"tsbm" -> context.resources.getString(R.string.verbum_tsbm)
						else -> dictionaryId
					}

					Article().apply {
						this.title = title
						this.description = description
						this.dictionary = "$dictionary $domain"
					}
				}

			if (articles.isNotEmpty()) {
				val pagination = jsonResponse.get("Pagination")
				val current = pagination.asJsonObject.get("Current").asInt
				val total = pagination.asJsonObject.get("Total").asInt

				if (current < total) {
					lastPage = false
				}
			}
		} catch (e: Exception) {
			notifier.debug("Unable to parse Verbum response: ${e.message}")
			articles = emptyList()
		}

		return Page(articles, lastPage)
	}

	private data class Page(val articles: List<Article>, val last: Boolean)
}