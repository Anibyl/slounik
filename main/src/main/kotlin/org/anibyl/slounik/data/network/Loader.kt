package org.anibyl.slounik.data.network

import kotlinx.coroutines.flow.Flow
import org.anibyl.slounik.data.ArticleBatch

interface Loader {
	fun findArticles(wordToSearch: String): Flow<ArticleBatch>
}