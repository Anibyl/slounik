package org.anibyl.slounik.data

import android.content.SearchRecentSuggestionsProvider

class SearchHistoryProvider : SearchRecentSuggestionsProvider() {
    init {
        setupSuggestions(AUTHORITY, MODE)
    }

    companion object {
        const val AUTHORITY: String = "org.anibyl.slounik.data.SearchHistoryProvider"
        const val MODE: Int = DATABASE_MODE_QUERIES
    }
}
