package org.anibyl.slounik.data.network

import android.content.Context
import android.net.Uri
import com.android.volley.RequestQueue
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.anibyl.slounik.Notifier
import org.anibyl.slounik.R
import org.anibyl.slounik.data.Article
import org.anibyl.slounik.data.ArticleBatch
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

@Singleton
class RodnyjaVobrazyLoader @Inject constructor(
	@ApplicationContext private val context: Context,
	private val notifier: Notifier,
	private val queue: RequestQueue
) : Loader {
	private val url: String = "rv-blr.com"

	override fun findArticles(wordToSearch: String) = flow {
		emit(
			getBatch(
				wordToSearch,
				explanatoryUri(wordToSearch),
				context.resources.getString(R.string.rodnyja_vobrazy_dictionary_explanatory)
			)
		)
		emit(
			getBatch(
				wordToSearch,
				ethnographyUri(wordToSearch),
				context.resources.getString(R.string.rodnyja_vobrazy_dictionary_ethnography)
			)
		)
		emit(
			getBatch(
				wordToSearch,
				mythologyUri(wordToSearch),
				context.resources.getString(R.string.rodnyja_vobrazy_dictionary_mythology)
			)
		)
		emit(
			getBatch(
				wordToSearch,
				redListUri(wordToSearch),
				context.resources.getString(R.string.rodnyja_vobrazy_dictionary_red_list)
			)
		)
	}.flowOn(Dispatchers.IO)

	private fun getBatch(wordToSearch: String, uri: Uri, dictionaryTitle: String): ArticleBatch {
		val article = try {
			getArticle(uri, wordToSearch)?.apply {
				dictionary = "$dictionaryTitle $url"
			}
		} catch (e: Exception) {
			notifier.debug("Error getting an article for $uri: ${e.message}")
			return ArticleBatch()
		}

		return ArticleBatch(article)
	}

	private fun getArticle(uri: Uri, wordToSearch: String): Article? {
		try {
			val response = queue.loadString(uri)

			val page = Jsoup.parse(response)
			val articleElements: Elements = page.select("table")

			val article: Article? = if (articleElements.size < 1) {
				null
			} else {
				try {
					parseElement(articleElements.first()!!, wordToSearch)
				} catch (e: IncorrectTitleException) {
					null
				}
			}

			return article
		} catch (e: Exception) {
			notifier.debug("Error response for $uri: ${e.message}")
			return null
		}
	}

	private fun parseElement(element: Element, wordToSearch: String?): Article {
		return Article().apply {
			val tds: Elements = element.select("td")

			when (tds.size) {
				3 -> {
					// Explanatory.
					title = tds[0].html()
						.replace(oldValue = "<b>", newValue = "", ignoreCase = true)
						.replace(oldValue = "</b>", newValue = "", ignoreCase = true)
						.trim()

					checkTitle(title, wordToSearch)

					description = tds[1].html() + "<br>" + tds[2].html()
				}

				1, 2 -> {
					// Other dictionaries.
					val td: Element? = tds.last()

					title = td!!.select("span").first()!!.ownText().trim()

					checkTitle(title, wordToSearch)

					description = td.html()

					val linkToFullDescription = td.select("a").attr("href")

					fullDescriptionLoader = {
						val response = queue.loadString(linkToFullDescription)
						val articlePage = Jsoup.parse(response)

						var fullArticleDescription = ""

						val titleTd: Element? = articlePage.select("td[class$=text_title]").first()

						if (titleTd != null) {
							var tr: Element? = titleTd.parent()
							do {
								fullArticleDescription += tr!!.html() + "<br>"
								tr = tr.nextElementSibling()
							} while (tr != null)
						}

						fullArticleDescription
					}
				}
			}
		}
	}

	private fun checkTitle(title: String?, wordToSearch: String?) {
		if (!wordToSearch.equals(title, ignoreCase = true)) {
			throw IncorrectTitleException("Incorrect article title $title for search $wordToSearch.")
		}
	}

	private fun explanatoryUri(wordToSearch: String): Uri {
		return Uri.Builder()
			.scheme("http")
			.authority(url)
			.appendPath("dictionary")
			.appendPath("searchWords")
			.appendQueryParameter("searchKeyword", wordToSearch)
			.build()
	}

	private fun ethnographyUri(wordToSearch: String): Uri {
		return getRequestString(wordToSearch, 0)
	}

	private fun mythologyUri(wordToSearch: String): Uri {
		return getRequestString(wordToSearch, 1)
	}

	private fun redListUri(wordToSearch: String): Uri {
		return getRequestString(wordToSearch, 2)
	}

	private fun getRequestString(wordToSearch: String, type: Int): Uri {
		return Uri.Builder()
			.scheme("http")
			.authority(url)
			.appendPath("dictionary")
			.appendPath("searchDictionaryWords")
			.appendQueryParameter("type", type.toString())
			.appendQueryParameter("searchKeyword", wordToSearch)
			.build()
	}
}