package org.anibyl.slounik.data.network

class IncorrectTitleException(s: String) : RuntimeException(s)
