package org.anibyl.slounik.data.network

import android.net.Uri
import com.android.volley.RequestQueue
import com.android.volley.toolbox.RequestFuture
import com.android.volley.toolbox.StringRequest

fun RequestQueue.loadString(uri: Uri): String {
	return this.loadString(uri.toString())
}

fun RequestQueue.loadString(uriStr: String): String {
	val future = RequestFuture.newFuture<String>()
	val request = StringRequest(uriStr, future, future)
	this.add(request)
	return future.get()
}