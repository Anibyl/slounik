package org.anibyl.slounik.data

class ArticleBatch(val list: List<Article>) {
	constructor() : this(emptyList())
	constructor(article: Article?) : this(if (article == null) emptyList() else listOf(article))
}
