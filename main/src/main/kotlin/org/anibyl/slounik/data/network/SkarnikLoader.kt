package org.anibyl.slounik.data.network

import android.content.Context
import android.net.Uri
import com.android.volley.NetworkResponse
import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.anibyl.slounik.Notifier
import org.anibyl.slounik.R
import org.anibyl.slounik.data.Article
import org.anibyl.slounik.data.ArticleBatch
import org.jsoup.Jsoup

@Singleton
class SkarnikLoader @Inject constructor(
	@ApplicationContext private val context: Context,
	private val notifier: Notifier,
	private val queue: RequestQueue,
) : Loader {
	private val label: String = "skarnik.by"
	private val domain: String = "www.skarnik.by"

	override fun findArticles(wordToSearch: String): Flow<ArticleBatch> = flow {
		emit(
			getBatch(
				wordToSearch,
				uri(wordToSearch, "rus"),
				context.resources.getString(R.string.skarnik_dictionary_rus_bel)
			)
		)
		emit(
			getBatch(
				wordToSearch,
				uri(wordToSearch, "bel"),
				context.resources.getString(R.string.skarnik_dictionary_bel_rus)
			)
		)
		emit(
			getBatch(
				wordToSearch,
				uri(wordToSearch, "beld"),
				context.resources.getString(R.string.skarnik_dictionary_explanatory)
			)
		)
	}.flowOn(Dispatchers.IO)

	// each batch contains one article
	private fun getBatch(wordToSearch: String, uri: Uri, dictionaryTitle: String): ArticleBatch {
		val articleDescription = try {
			getArticleDescription(uri)
		} catch (e: Exception) {
			notifier.error("Unable to load description for $uri", e)
			return ArticleBatch()
		}

		return ArticleBatch(
			articleDescription?.let { description ->
				Article().apply {
					this.description = description
					this.title = wordToSearch
					this.dictionary = "$dictionaryTitle $label"
				}
			}
		)
	}

	private fun getArticleDescription(uri: Uri): String? {
		try {
			val response = queue.loadString(uri)
			return parsePage(response)
		} catch(e: Exception) {
			if (e.cause is VolleyError) {
				val error = e.cause as VolleyError

				notifier.error("Error response for $uri", error)

				val networkResponse: NetworkResponse? = error.networkResponse

				if (networkResponse != null && networkResponse.statusCode == 302) {
					/* Skarnik has a strange redirection behaviour:
					   http search → https search → http page → https page.
					   I skip first and third parts to get
					   https search → https page. */
					val location: String? = networkResponse.headers?.get("Location")
					if (location != null) {
						if (location.startsWith("http:")) {
							val response = queue.loadString(location.replace("http:", "https:"))
							return parsePage(response)
						} else {
							// Should not happen.
							val response = queue.loadString(location)
							return parsePage(response)
						}
					} else {
						// Should not happen.
						return null
					}
				} else {
					return null
				}
			} else {
				throw e
			}
		}
	}

	private fun parsePage(response: String): String? {
		val page = Jsoup.parse(response)
		val articleElements = page.select("p#trn")

		return if (articleElements.size == 0) {
			null
		} else {
			articleElements.first()!!.html()
		}
	}

	private fun uri(wordToSearch: String, language: String): Uri {
		return Uri.Builder()
			.scheme("https")
			.authority(domain)
			.appendPath("search")
			.appendQueryParameter("lang", language)
			.appendQueryParameter("term", wordToSearch)
			.build()
	}
}
