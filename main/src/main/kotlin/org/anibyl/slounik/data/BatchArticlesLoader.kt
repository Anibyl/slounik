package org.anibyl.slounik.data

import com.android.volley.RequestQueue
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.merge
import org.anibyl.slounik.core.Preferences
import org.anibyl.slounik.data.network.Loader
import org.anibyl.slounik.data.network.RodnyjaVobrazyLoader
import org.anibyl.slounik.data.network.SkarnikLoader
import org.anibyl.slounik.data.network.SlounikOrgLoader
import org.anibyl.slounik.data.network.VerbumLoader

@Singleton
class BatchArticlesLoader @Inject constructor(
	private val queue: RequestQueue,
	private val preferences: Preferences,
	private val rodnyjaVobrazyLoader: RodnyjaVobrazyLoader,
	private val skarnik: SkarnikLoader,
	private val slounikOrg: SlounikOrgLoader,
	private val verbum: VerbumLoader,
) {
	fun loadArticles(wordToSearch: String): Flow<ArticleBatch> {
		cancel()

		return activeLoaders()
			.map { loader -> loader.findArticles(wordToSearch) }
			.merge()
	}

	fun cancel() {
		queue.cancelAll("cancel")
	}

	private fun activeLoaders(): Collection<Loader> {
		val result = arrayListOf<Loader>()

		if (preferences.useRodnyjaVobrazy) result.add(rodnyjaVobrazyLoader)
		if (preferences.useSkarnik) result.add(skarnik)
		if (preferences.useSlounikOrg) result.add(slounikOrg)
		if (preferences.useVerbum) result.add(verbum)

		return result
	}
}
