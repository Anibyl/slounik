package org.anibyl.slounik.data

import android.text.Spanned
import java.io.Serializable
import org.anibyl.slounik.core.fromHtml

class Article : Serializable {
	var title: String? = null
		internal set
	var description: String? = null
		internal set
	var dictionary: String? = null
		internal set
	var fullDescription: String? = null
		internal set
	@Transient
	var fullDescriptionLoader: (() -> String)? = null
		internal set

	val spannedDescription: Spanned?
		get() = description?.fromHtml()
	val spannedFullDescription: Spanned?
		get() = fullDescription?.fromHtml()

	fun loadFullDescription() {
		fullDescription = fullDescriptionLoader?.invoke()
	}

	fun canLoadFullDescription(): Boolean {
		return fullDescriptionLoader != null
	}
}
