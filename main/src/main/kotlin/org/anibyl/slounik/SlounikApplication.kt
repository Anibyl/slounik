package org.anibyl.slounik

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject
import org.anibyl.slounik.ui.ThemeService

@HiltAndroidApp
class SlounikApplication : Application() {
	@Inject lateinit var themeService: ThemeService

	override fun onCreate() {
		super.onCreate()

		themeService.setDefault()
	}
}
