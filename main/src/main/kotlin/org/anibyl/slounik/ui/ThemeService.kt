package org.anibyl.slounik.ui

import androidx.appcompat.app.AppCompatDelegate
import javax.inject.Inject
import javax.inject.Singleton
import org.anibyl.slounik.core.Preferences

@Singleton
class ThemeService @Inject constructor(private val preferences: Preferences) {
    fun set(theme: Theme) {
        preferences.theme = theme

        when (theme) {
            Theme.Dark -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            Theme.Follow -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
            Theme.Light -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }

    fun setDefault() {
        set(preferences.theme)
    }
}

enum class Theme { Dark, Follow, Light }
