package org.anibyl.slounik.activities

import android.provider.SearchRecentSuggestions
import androidx.lifecycle.lifecycleScope
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.flow.takeWhile
import kotlinx.coroutines.launch
import org.anibyl.slounik.Notifier
import org.anibyl.slounik.data.Article
import org.anibyl.slounik.data.BatchArticlesLoader
import org.anibyl.slounik.data.SearchHistoryProvider

@Singleton
class SlounikActivityPresenter @Inject constructor(
	private val notifier: Notifier,
	private val loader: BatchArticlesLoader,
) {
	internal val articles: ArrayList<Article> = arrayListOf()

	internal var searching = false

	private var activity: SlounikActivity? = null

	internal fun onActivityCreated(slounikActivity: SlounikActivity) {
		this.activity = slounikActivity
	}

	internal fun onActivityDestroyed() {
		this.activity = null
	}

	internal fun onSearchClicked(wordToSearch: String) {
		if (searching) {
			onStopSearchClicked()
		}

		articles.clear()
		activity?.articlesUpdated()

		val preparedWord: String = wordToSearch.trim()

		if (preparedWord.isEmpty()) {
			notifier.toast("Nothing to search.")
		} else {
			searching = true
			activity?.searchStarted()

			activity?.lifecycleScope?.launch {
				loader.loadArticles(preparedWord)
					.takeWhile { searching }
					.collect {
						articles.addAll(it.list)
						activity?.articlesUpdated()
					}

				searching = false
				activity?.searchEnded()
			}

			SearchRecentSuggestions(activity, SearchHistoryProvider.AUTHORITY, SearchHistoryProvider.MODE)
				.saveRecentQuery(wordToSearch, null)
		}
	}

	internal fun onStopSearchClicked() {
		loader.cancel()

		searching = false
		activity?.searchEnded()
	}
}
