package org.anibyl.slounik.activities

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.core.view.MenuProvider
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import org.anibyl.slounik.R
import org.anibyl.slounik.core.Preferences
import org.anibyl.slounik.databinding.DrawerBinding
import org.anibyl.slounik.dialogs.AboutDialog
import org.anibyl.slounik.ui.Theme
import org.anibyl.slounik.ui.ThemeService

@AndroidEntryPoint
class NavigationDrawerFragment : Fragment(), MenuProvider {
	interface NavigationDrawerCallbacks {
		fun getSupportActionBar(): ActionBar
	}

	@Inject lateinit var preferences: Preferences
	@Inject lateinit var themeService: ThemeService

	private lateinit var binding: DrawerBinding

	private val actionBar: ActionBar
		get() = callbacks.getSupportActionBar()

	private lateinit var callbacks: NavigationDrawerCallbacks

	private var drawerToggle: ActionBarDrawerToggle? = null

	private var drawerLayout: DrawerLayout? = null
	private var fragmentContainerView: View? = null

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		requireActivity().addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		binding = DrawerBinding.inflate(inflater, container, false)

		// TODO Create list with disabling functionality.
		binding.checkboxSlounikOrg.isChecked = preferences.useSlounikOrg
		binding.checkboxSlounikOrg.setOnCheckedChangeListener { _, isChecked ->
			preferences.useSlounikOrg = isChecked
		}

		binding.checkboxSkarnik.isChecked = preferences.useSkarnik
		binding.checkboxSkarnik.setOnCheckedChangeListener { _, isChecked ->
			preferences.useSkarnik = isChecked
		}

		binding.checkboxRodnyjaVobrazy.isChecked = preferences.useRodnyjaVobrazy
		binding.checkboxRodnyjaVobrazy.setOnCheckedChangeListener { _, isChecked ->
			preferences.useRodnyjaVobrazy = isChecked
		}

		binding.checkboxVerbum.isChecked = preferences.useVerbum
		binding.checkboxVerbum.setOnCheckedChangeListener { _, isChecked ->
			preferences.useVerbum= isChecked
		}

		binding.checkboxSearchInTitle.isChecked = preferences.searchInTitles
		binding.checkboxSearchInTitle.setOnCheckedChangeListener { _, isChecked ->
			preferences.searchInTitles = isChecked
		}

		binding.themeRadioGroup.check(
			when (preferences.theme) {
				Theme.Dark -> binding.themeDark.id
				Theme.Follow -> binding.themeFollow.id
				Theme.Light -> binding.themeLight.id
			}
		)
		binding.themeRadioGroup.setOnCheckedChangeListener { _, checkedId ->
			when (checkedId) {
				binding.themeDark.id -> themeService.set(Theme.Dark)
				binding.themeFollow.id -> themeService.set(Theme.Follow)
				binding.themeLight.id -> themeService.set(Theme.Light)
			}
		}

		binding.aboutButton.setOnClickListener {
			if (activity != null) {
				AboutDialog().show(requireActivity().supportFragmentManager, "about_dialog")
			}
		}

		return binding.root
	}

	override fun onAttach(context: Context) {
		super.onAttach(context)

		try {
			callbacks = context as NavigationDrawerCallbacks
		} catch (e: ClassCastException) {
			throw ClassCastException("Activity must implement NavigationDrawerCallbacks.")
		}
	}

	override fun onConfigurationChanged(newConfig: Configuration) {
		super.onConfigurationChanged(newConfig)
		// Forward the new configuration the drawer toggle component.
		drawerToggle!!.onConfigurationChanged(newConfig)
	}

	fun close() {
		if (drawerLayout != null && fragmentContainerView != null) {
			drawerLayout!!.closeDrawer(fragmentContainerView!!)
		}
	}

	internal fun setup(fragmentContainerView: View, drawerLayout: DrawerLayout) {
		this.fragmentContainerView = fragmentContainerView
		this.drawerLayout = drawerLayout

		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START)

		val actionBar = actionBar
		actionBar.setDisplayHomeAsUpEnabled(true)
		actionBar.setHomeButtonEnabled(true)

		drawerToggle = object : ActionBarDrawerToggle(
				activity,
				drawerLayout,
				R.string.navigation_drawer_open,
				R.string.navigation_drawer_close
		) {
			override fun onDrawerClosed(drawerView: View) {
				super.onDrawerClosed(drawerView)

				if (isAdded) {
					activity?.invalidateOptionsMenu()
				}
			}

			override fun onDrawerOpened(drawerView: View) {
				super.onDrawerOpened(drawerView)

				if (isAdded) {
					activity?.invalidateOptionsMenu()
				}
			}
		}

		drawerLayout.post { drawerToggle!!.syncState() }

		drawerLayout.addDrawerListener(drawerToggle!!)
	}

	override fun onCreateMenu(menu: Menu, inflater: MenuInflater) {
	}

	override fun onMenuItemSelected(item: MenuItem): Boolean {
		if (drawerToggle!!.onOptionsItemSelected(item)) {
			return true
		}

		close()

		return false
	}
}
