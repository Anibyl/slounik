package org.anibyl.slounik.activities

import android.app.SearchManager
import android.app.SearchManager.SUGGEST_COLUMN_QUERY
import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.Menu
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.SearchView.GONE
import android.widget.SearchView.OnQueryTextListener
import android.widget.SearchView.OnSuggestionListener
import android.widget.SearchView.VISIBLE
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import org.anibyl.slounik.R
import org.anibyl.slounik.data.Article
import org.anibyl.slounik.databinding.MainBinding
import org.anibyl.slounik.dialogs.newArticleDialog

@AndroidEntryPoint
class SlounikActivity : AppCompatActivity(), NavigationDrawerFragment.NavigationDrawerCallbacks {
	@Inject lateinit var presenter: SlounikActivityPresenter

	private lateinit var binding: MainBinding

	private val articles: List<Article>
		get() = presenter.articles

	private lateinit var navigationDrawerFragment: NavigationDrawerFragment
	private lateinit var adapter: SlounikAdapter
	private lateinit var inputMethodManager: InputMethodManager
	private var largeArticlesAmountFont:Boolean = true

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		binding = MainBinding.inflate(layoutInflater)
		setContentView(binding.root)

		setSupportActionBar(binding.toolbar)

		navigationDrawerFragment = supportFragmentManager.findFragmentById(R.id.navigation_drawer)
				as NavigationDrawerFragment
		adapter = SlounikAdapter(this, R.layout.list_item, R.id.list_item_description, articles)
		inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager

		binding.stopButton.setOnClickListener {
			presenter.onStopSearchClicked()
		}

		binding.listView.adapter = adapter
		binding.listView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
			try {
				newArticleDialog(articles[position]).show(supportFragmentManager, "article_dialog")
			} catch (t: Throwable) {
				// who cares?
			}
		}

		updateArticlesAmount()

		updateProgress()

		navigationDrawerFragment.setup(
				findViewById(R.id.navigation_drawer),
				findViewById(R.id.drawer_layout)
		)

		presenter.onActivityCreated(this)
	}

	override fun onDestroy() {
		presenter.onActivityDestroyed()
		super.onDestroy()
	}

	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

		binding.searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))

		binding.searchView.setOnQueryTextFocusChangeListener { view, hasFocus ->
			if (hasFocus) {
				inputMethodManager.showSoftInput(view.findFocus(), 0)
				navigationDrawerFragment.close()
			} else {
				inputMethodManager.hideSoftInputFromWindow(binding.searchView.windowToken, 0)
				navigationDrawerFragment.close()
			}
		}

		binding.searchView.setOnQueryTextListener(object : OnQueryTextListener {
			override fun onQueryTextSubmit(query: String?): Boolean {
				if (query != null) {
					onQuerySubmit(query)
				}
				return true
			}

			override fun onQueryTextChange(newText: String?): Boolean {
				navigationDrawerFragment.close()
				return true
			}
		})

		binding.searchView.setOnSuggestionListener(object : OnSuggestionListener {
			override fun onSuggestionSelect(position: Int): Boolean = false // never works

			override fun onSuggestionClick(position: Int): Boolean {
				val query = binding.searchView.suggestionsAdapter.cursor
					.apply { moveToPosition(position) }
					.let { it.getString(it.getColumnIndex(SUGGEST_COLUMN_QUERY)) }

				binding.searchView.setQuery(query, false)
				onQuerySubmit(query)
				return true
			}
		})

		return true
	}

	override fun getSupportActionBar(): ActionBar {
		return super.getSupportActionBar()!!
	}

	internal fun searchStarted() {
		binding.progress.progressiveStart()
		binding.stopButton.visibility = VISIBLE
	}

	internal fun searchEnded() {
		binding.progress.progressiveStop()
		binding.stopButton.visibility = GONE
	}

	internal fun articlesUpdated() {
		updateArticlesAmount()
		adapter.notifyDataSetChanged()
	}

	private fun onQuerySubmit(query: String) {
		binding.searchView.clearFocus()
		binding.listView.requestFocus()
		navigationDrawerFragment.close()
		presenter.onSearchClicked(query)
	}

	private fun updateArticlesAmount() {
		binding.articlesCounter.text = articles.size.toString()

		when {
			!largeArticlesAmountFont && articles.size in 0..999 -> {
				binding.articlesCounter.setTextSize(
						TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.background_counter_bigger_font_size)
				)
				largeArticlesAmountFont = true
			}
			largeArticlesAmountFont && articles.size > 999 -> {
				binding.articlesCounter.setTextSize(
						TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.background_counter_font_size)
				)
				largeArticlesAmountFont = false
			}
		}
	}

	private fun updateProgress() {
		if (presenter.searching) {
			binding.progress.progressiveStart()
		} else {
			binding.progress.progressiveStop()
		}
	}
}
