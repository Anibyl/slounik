package org.anibyl.slounik.core

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton
import org.anibyl.slounik.ui.Theme

private const val USE_SLOUNIK_ORG = "use_slounik_org"
private const val USE_SKARNIK = "use_skarnik"
private const val USE_RODNYJA_VOBRAZY = "use_rodnyja_vobrazy"
private const val USE_VERBUM = "use_verbum"
private const val SEARCH_IN_TITLES = "search_in_titles"
private const val THEME = "theme"

@Singleton
class Preferences @Inject constructor(@ApplicationContext context: Context) {
	private val manager: PreferencesManager = PreferencesManager(
			context.getSharedPreferences("org.anibyl.slounik", Context.MODE_PRIVATE)
	)

	var useSlounikOrg: Boolean
		get() = manager.getBoolean(USE_SLOUNIK_ORG, true)
		set(useSlounikOrg) {
			manager.save(USE_SLOUNIK_ORG, useSlounikOrg)
		}

	var useSkarnik: Boolean
		get() = manager.getBoolean(USE_SKARNIK, true)
		set(useSkarnik) {
			manager.save(USE_SKARNIK, useSkarnik)
		}

	var useRodnyjaVobrazy: Boolean
		get() = manager.getBoolean(USE_RODNYJA_VOBRAZY, true)
		set(useRodnyjaVobrazy) {
			manager.save(USE_RODNYJA_VOBRAZY, useRodnyjaVobrazy)
		}

	var useVerbum: Boolean
		get() = manager.getBoolean(USE_VERBUM, true)
		set(useRodnyjaVobrazy) = manager.save(USE_VERBUM, useRodnyjaVobrazy)

	var searchInTitles: Boolean
		get() = manager.getBoolean(SEARCH_IN_TITLES)
		set(searchInTitles) {
			manager.save(SEARCH_IN_TITLES, searchInTitles)
		}

	var theme: Theme
		get() = manager.getString(THEME).let { s -> if (s != null) Theme.valueOf(s) else Theme.Follow }
		set(value) = manager.save(THEME, value.name)

	private class PreferencesManager(private val sharedPreferences: SharedPreferences) {
		fun save(key: String, value: Boolean) {
			apply(edit().putBoolean(key, value))
		}

		fun save(key: String, value: String?) {
			apply(edit().putString(key, value))
		}

		fun getBoolean(key: String): Boolean {
			return sharedPreferences.getBoolean(key, false)
		}

		fun getBoolean(key: String, defaultValue: Boolean): Boolean {
			return sharedPreferences.getBoolean(key, defaultValue)
		}

		fun getString(key: String): String? {
			return sharedPreferences.getString(key, null)
		}

		fun edit(): SharedPreferences.Editor {
			return sharedPreferences.edit()
		}

		fun apply(editor: SharedPreferences.Editor) {
			editor.apply()
		}
	}
}
