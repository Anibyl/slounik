package org.anibyl.slounik

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.annotation.StringRes
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "Slounik"

@Singleton
class Notifier @Inject constructor(@ApplicationContext private val context: Context) {
	fun toast(@StringRes id: Int, developerMode: Boolean = false) {
		toast(context.resources.getString(id), developerMode)
	}

	fun toast(text: CharSequence, developerMode: Boolean = false, length: Int = Toast.LENGTH_SHORT) {
		if (!developerMode || BuildConfig.DEBUG) {
			Toast.makeText(context, text, length).show()
		}
	}

	fun debug(message: String) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, message)
		}
	}

	fun error(message: String, throwable: Throwable) {
		if (BuildConfig.DEBUG) {
			Log.e(TAG, message, throwable)
		}
	}
}
