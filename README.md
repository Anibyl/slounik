# Slounik

**Slounik** is an Android client application for Belarusian dictionary websites published on
[Google Play](https://play.google.com/store/apps/details?id=org.anibyl.slounik).

Currently, it supports dictionaries from following websites:
* [slounik.org](https://slounik.org) (Электронная энцыкляпэдыя),
* [skarnik.by](https://skarnik.by) (Скарнік),
* [rv-blr.com](http://rv-blr.com/dictionary/list) (Родныя вобразы),
* [verbum.by](https://verbum.by) (Verbum, [source](https://github.com/verbumby/verbum)).

## Issues

If you want to submit an issue use [this page](https://gitlab.com/Anibyl/slounik/-/issues).

## Note

This is my hobby project, so I might not update it as frequently as possible.
