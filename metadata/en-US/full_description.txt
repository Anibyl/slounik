Client application for searching at Belarusian dictionary websites.

It supports dictionaries from these sources:
· slounik.org (Электронная энцыкляпэдыя),
· skarnik.by (Скарнік),
· rv-blr.com (Родныя вобразы),
· verbum.by (Verbum).
