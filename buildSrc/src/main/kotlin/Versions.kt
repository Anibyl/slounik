import org.gradle.api.JavaVersion

object Versions {
	const val androidGradle = "8.2.2"
	const val kotlin = "1.9.22"
	const val hilt = "2.51"
	val java = JavaVersion.VERSION_17
}
