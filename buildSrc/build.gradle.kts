plugins {
	`kotlin-dsl`
	kotlin("jvm") version "1.9.22"
}

repositories {
	mavenCentral()
}
dependencies {
	implementation(kotlin("stdlib"))
}
